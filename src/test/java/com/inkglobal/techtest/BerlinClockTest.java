package com.inkglobal.techtest;


import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BerlinClockTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testConvertToBerlinTime() {
		Assert.assertEquals(TEST1RESULT, BerlinClock.convertToBerlinTime(TEST1));
		Assert.assertEquals(TEST2RESULT, BerlinClock.convertToBerlinTime(TEST2));
		Assert.assertEquals(TEST3RESULT, BerlinClock.convertToBerlinTime(TEST3));
		Assert.assertEquals(TEST4RESULT, BerlinClock.convertToBerlinTime(TEST4));
	}

	@Test
	public void testSecondsBlinking() {
		Assert.assertEquals(BerlinClock.OFF, BerlinClock.getSeconds(1));
		Assert.assertEquals(BerlinClock.YELLOW, BerlinClock.getSeconds(0));
	}

	@Test 
	public void testHoursFirstRow() {
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(0));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(1));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(2));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(3));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(4));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(5));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.RED+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursFirstRow(10));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.RED+BerlinClock.RED+BerlinClock.OFF, BerlinClock.getHoursFirstRow(15));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.RED+BerlinClock.RED+BerlinClock.RED, BerlinClock.getHoursFirstRow(20));
	}
	
	@Test
	public void testHoursSecondRow() {
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursSecondRow(0));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursSecondRow(1));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.RED+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursSecondRow(2));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.RED+BerlinClock.RED+BerlinClock.OFF, BerlinClock.getHoursSecondRow(3));
		Assert.assertEquals(BerlinClock.RED+BerlinClock.RED+BerlinClock.RED+BerlinClock.RED, BerlinClock.getHoursSecondRow(4));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getHoursSecondRow(5));
	}

	@Test 
	public void testMinutesFirstRow() {
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(0).substring(0, 4));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(1).substring(0, 4));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(2).substring(0, 4));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(3).substring(0, 4));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(4).substring(0, 4));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(5).substring(0, 4));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(10).substring(0, 4));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.RED+BerlinClock.OFF, BerlinClock.getMinutesFirstRow(15).substring(0, 4));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.RED+BerlinClock.YELLOW, BerlinClock.getMinutesFirstRow(20).substring(0, 4));
	}

	@Test
	public void testMinutesSecondRow() {
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesSecondRow(0));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesSecondRow(1));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesSecondRow(2));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.OFF, BerlinClock.getMinutesSecondRow(3));
		Assert.assertEquals(BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.YELLOW+BerlinClock.YELLOW, BerlinClock.getMinutesSecondRow(4));
		Assert.assertEquals(BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF+BerlinClock.OFF, BerlinClock.getMinutesSecondRow(5));
	}

	@Test
	public void testValidationInvalidInput() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("junk");
	}

	@Test
	public void testValidationHoursTooLow() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("-1:00:00");
	}

	@Test
	public void testValidationHoursTooHigh() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("25:00:00");
	}

	@Test
	public void testValidationHoursNonNumeric() {
		exception.expect(NumberFormatException.class);
		BerlinClock.convertToBerlinTime("A:00:00");
	}
	
	@Test
	public void testValidationMinutesTooLow() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("00:-1:00");
	}

	@Test
	public void testValidationMinutesTooHigh() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("00:60:00");
	}

	@Test
	public void testValidationMinutesNonNumeric() {
		exception.expect(NumberFormatException.class);
		BerlinClock.convertToBerlinTime("00:A:00");
	}

	@Test
	public void testValidationSecondsTooLow() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("00:00:-1");
	}

	@Test
	public void testValidationSecondsTooHigh() {
		exception.expect(IllegalArgumentException.class);
		BerlinClock.convertToBerlinTime("00:00:60");
	}

	@Test
	public void testValidationSecondsNonNumeric() {
		exception.expect(NumberFormatException.class);
		BerlinClock.convertToBerlinTime("00:00:A");
	}

	private static String TEST1 = "00:00:00";
	private static String TEST1RESULT = BerlinClock.YELLOW
		+ BerlinClock.SEPERATOR
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.SEPERATOR
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.SEPERATOR
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.SEPERATOR
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF
		+ BerlinClock.OFF;

	private static String TEST2 = "13:17:01";
	private static String TEST2RESULT = BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.QUARTROWREPLACE
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.YELLOW
			+ BerlinClock.YELLOW
			+ BerlinClock.OFF
			+ BerlinClock.OFF;

	private static String TEST3 = "23:59:59";
	private static String TEST3RESULT = BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.SEPERATOR
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.QUARTROWREPLACE
			+ BerlinClock.QUARTROWREPLACE
			+ BerlinClock.QUARTROWREPLACE
			+ BerlinClock.YELLOW
			+ BerlinClock.YELLOW
			+ BerlinClock.SEPERATOR
			+ BerlinClock.YELLOW
			+ BerlinClock.YELLOW
			+ BerlinClock.YELLOW
			+ BerlinClock.YELLOW;

	private static String TEST4 = "24:00:00";
	private static String TEST4RESULT = BerlinClock.YELLOW
			+ BerlinClock.SEPERATOR
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.SEPERATOR
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.RED
			+ BerlinClock.SEPERATOR
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.SEPERATOR
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF
			+ BerlinClock.OFF;

}
