package com.inkglobal.techtest;

import java.util.ArrayList;
import java.util.List;

public class BerlinClock {

	public static final String SEPERATOR = "\n";
	public static final String RED = "R";
	public static final String YELLOW = "Y";
	public static final String OFF = "O";
	public static final String QUARTROWORIG = YELLOW+YELLOW+YELLOW;
	public static final String QUARTROWREPLACE = YELLOW+YELLOW+RED;

	private static final int HOURS = 0;
	private static final int MINUTES = 1;
	private static final int SECONDS = 2;

	/**
	 *	Converts time to Berlin Clock notation.
	 * 
	 *	@param time
	 *		String containing the time in HH:MM:SS, may be null.
	 *
	 *	@return Berlin Clock notation text or {@code null} if null String input.
	 */
	public static String convertToBerlinTime(String time) {
		if ((time==null) || !time.contains(":")) {
			throw new IllegalArgumentException("Illegal Arguments: time is null");
		}
		List<Integer> timeParts = new ArrayList<Integer>();
		for (String part : time.split(":")) {
			timeParts.add(Integer.parseInt(part));
		}
		if (timeParts.size() != 3) {
			throw new IllegalArgumentException("Illegal Arguments: expected (xx:xx:xx) but got "+ time);
		}
		if ((timeParts.get(HOURS) < 0) || (timeParts.get(HOURS) > 24)) {
			throw new IllegalArgumentException("Illegal Arguments: hours is out of range "+ timeParts.get(HOURS));
		}
		if ((timeParts.get(MINUTES) < 0) || (timeParts.get(MINUTES) > 59)) {
			throw new IllegalArgumentException("Illegal Arguments: minutes is out of range "+ timeParts.get(HOURS));
		}
		if ((timeParts.get(SECONDS) < 0) || (timeParts.get(SECONDS) > 59)) {
			throw new IllegalArgumentException("Illegal Arguments: seconds is out of range "+ timeParts.get(HOURS));
		}
		StringBuilder result = new StringBuilder();
		result.append(getSeconds(timeParts.get(SECONDS)))
			.append(SEPERATOR)
			.append(getHoursFirstRow(timeParts.get(HOURS)))
			.append(SEPERATOR)
			.append(getHoursSecondRow(timeParts.get(HOURS)))
			.append(SEPERATOR)
			.append(getMinutesFirstRow(timeParts.get(MINUTES)))
			.append(SEPERATOR)
			.append(getMinutesSecondRow(timeParts.get(MINUTES)));
		return result.toString();
	}

	protected static String getSeconds(int seconds) {
		return (seconds % 2 == 0) ? YELLOW : OFF;
	}

	protected static String getHoursFirstRow(int hours) {
		return getClockLamps(4, ((hours - (hours % 5)) / 5), RED, OFF);
	}

	protected static String getHoursSecondRow(int hours) {
		return getClockLamps(4, hours % 5, RED, OFF);
	}

	protected static String getMinutesFirstRow(int minutes) {
		return getClockLamps(11, ((minutes - (minutes % 5)) / 5), YELLOW, OFF).replaceAll(QUARTROWORIG, QUARTROWREPLACE);
	}

	protected static String getMinutesSecondRow(int minutes) {
		return getClockLamps(4, minutes % 5, YELLOW, OFF);
	}

	protected static String getClockLamps(int numberOfLamps, int onLamps, String onColour, String offColour) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < onLamps; i++) {
			result.append(onColour);
		}
		// small optimisation
		for (int i = (numberOfLamps - onLamps); i > 0 ; i--) {
			result.append(offColour);
		}
		return result.toString();
	}

}